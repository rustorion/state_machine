use anyhow::Result;
use std::fmt::Debug;
use std::sync::{Arc, Mutex};

/* TODO
	* RPC-pattern
	* Handle errors properly
*/

/* TODO
	* RPC-pattern
	* Handle errors properly
*/

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct IdType;

pub type MachineId = uid::IdU64<IdType>;

pub type EventSink<Event> = Box<dyn Fn(Event) -> Result<()> + Send + Sync>;

pub trait EventTrait: Debug + Clone + Send + Sync + 'static {}
impl<Event: Debug + Clone + Send + Sync + 'static> EventTrait for Event {}

pub trait CanSubscribe<Producer: AsyncEventMachine + ?Sized>: AsyncEventMachine {
	fn translate_event(event: Producer::OutEvent) -> Vec<Self::InEvent>;
}

pub struct TokioDriver<Machine: AsyncEventMachine> {
	pub handle: TokioHandle<Machine>,
	pub guts: TokioGuts<Machine>,
}

impl<Machine: AsyncEventMachine> Default for TokioDriver<Machine> {
	fn default() -> Self {
		let (sender, receiver) = tokio::sync::mpsc::unbounded_channel();
		TokioDriver {
			handle: TokioHandle {
				sender,
				subscribers: Default::default(),
				machine_id: Default::default(),
			},
			guts: TokioGuts { receiver },
		}
	}
}

pub trait Driver<Machine: AsyncEventMachine> {
	type Handle: Handle<Machine>;
	type Guts: Guts<Machine>;
}

impl<Machine: AsyncEventMachine> Driver<Machine> for TokioDriver<Machine> {
	type Handle = TokioHandle<Machine>;
	type Guts = TokioGuts<Machine>;
}

pub trait Handle<Machine: AsyncEventMachine>: Clone {
	/// Should be copied with clones
	fn machine_id(&self) -> MachineId;
	/// Subscribe another event machine to our events
	fn add_subscriber<Subscriber: CanSubscribe<Machine>>(&self, handle: &impl Handle<Subscriber>) -> bool;
	// Unsubscribe another event machine from our events
	fn remove_subscriber<Subscriber: CanSubscribe<Machine>>(&self, handle: &impl Handle<Subscriber>) -> bool;
	/// Submit an InEvent to the running machine
	fn send_event(&self, event: Machine::InEvent);

	fn event_sink(&self) -> EventSink<Machine::InEvent>;

	fn subscriber_sink<Producer: AsyncEventMachine>(&self) -> EventSink<Producer::OutEvent>
	where
		Machine: CanSubscribe<Producer>,
	{
		let event_sink = self.event_sink();
		Box::new(move |event| {
			let events: Vec<Machine::InEvent> = Machine::translate_event(event);
			events.into_iter().try_for_each(&event_sink)
		})
	}

	fn broadcast_event(&self, event: Machine::OutEvent);
}

pub trait Guts<Machine: AsyncEventMachine> {}

pub struct TokioHandle<Machine: AsyncEventMachine> {
	pub sender: tokio::sync::mpsc::UnboundedSender<Machine::InEvent>,
	pub subscribers: Arc<Mutex<std::collections::hash_map::HashMap<MachineId, EventSink<Machine::OutEvent>>>>,
	machine_id: MachineId,
}

impl<Machine: AsyncEventMachine> Clone for TokioHandle<Machine> {
	fn clone(&self) -> Self {
		TokioHandle {
			sender: self.sender.clone(),
			subscribers: self.subscribers.clone(),
			machine_id: self.machine_id,
		}
	}
}

pub struct TokioGuts<Machine: AsyncEventMachine> {
	pub receiver: tokio::sync::mpsc::UnboundedReceiver<Machine::InEvent>,
}

impl<Machine: AsyncEventMachine> Guts<Machine> for TokioGuts<Machine> {}

impl<Machine: AsyncEventMachine> Handle<Machine> for TokioHandle<Machine> {
	fn machine_id(&self) -> MachineId {
		self.machine_id
	}
	fn broadcast_event(&self, event: Machine::OutEvent) {
		let mut failed_keys = vec![];
		let mut subscribers = self.subscribers.lock().unwrap();
		for (key, subscriber) in subscribers.iter() {
			if subscriber(event.clone()).is_err() {
				failed_keys.push(*key);
			}
		}
		// TODO: warn about removing?
		for key in failed_keys {
			subscribers.remove(&key);
		}
	}
	fn add_subscriber<Subscriber: CanSubscribe<Machine>>(&self, handle: &impl Handle<Subscriber>) -> bool {
		self.subscribers.lock().unwrap().insert(handle.machine_id(), handle.subscriber_sink()).is_none()
	}

	fn remove_subscriber<Subscriber: CanSubscribe<Machine>>(&self, handle: &impl Handle<Subscriber>) -> bool {
		self.subscribers.lock().unwrap().remove(&handle.machine_id()).is_some()
	}

	fn event_sink(&self) -> EventSink<Machine::InEvent> {
		let sender = self.sender.clone();
		Box::new(move |event| sender.send(event).map_err(Into::into))
	}

	fn send_event(&self, event: Machine::InEvent) {
		self.sender.send(event).unwrap();
	}
}

pub trait AsyncEventMachine: Sized {
	type InEvent: EventTrait;
	type OutEvent: EventTrait;
	type Driver: Driver<Self>;
}

#[derive(Default)]
pub struct SimpleTokioMachine<Pair: AsyncEventMachine>(pub TokioDriver<Self>);

impl<Pair: AsyncEventMachine> AsyncEventMachine for SimpleTokioMachine<Pair> {
	type InEvent = Pair::OutEvent;
	type OutEvent = Pair::InEvent;
	type Driver = TokioDriver<Self>;
}

impl<Pair: AsyncEventMachine> CanSubscribe<Pair> for SimpleTokioMachine<Pair> {
	fn translate_event(event: Pair::OutEvent) -> Vec<Self::InEvent> {
		vec![event]
	}
}

impl<Pair: AsyncEventMachine> CanSubscribe<SimpleTokioMachine<Pair>> for Pair {
	fn translate_event(event: Self::InEvent) -> Vec<Pair::InEvent> {
		vec![event]
	}
}

pub struct FutureDriver<Machine: AsyncEventMachine> {
	pub handle: FutureHandle<Machine>,
	pub guts: FutureGuts<Machine>,
}

impl<Machine: AsyncEventMachine> Driver<Machine> for FutureDriver<Machine> {
	type Handle = FutureHandle<Machine>;
	type Guts = FutureGuts<Machine>;
}

impl<Machine: AsyncEventMachine> Default for FutureDriver<Machine> {
	fn default() -> Self {
		let (sender, receiver) = futures::channel::mpsc::unbounded();
		FutureDriver {
			handle: FutureHandle {
				sender,
				subscribers: Default::default(),
				machine_id: Default::default(),
			},
			guts: FutureGuts { receiver },
		}
	}
}

#[derive(Default)]
pub struct SimpleFutureMachine<Pair: AsyncEventMachine>(pub FutureDriver<Self>);

impl<Pair: AsyncEventMachine> AsyncEventMachine for SimpleFutureMachine<Pair> {
	type InEvent = Pair::OutEvent;
	type OutEvent = Pair::InEvent;
	type Driver = FutureDriver<Self>;
}

impl<Pair: AsyncEventMachine> CanSubscribe<Pair> for SimpleFutureMachine<Pair> {
	fn translate_event(event: Pair::OutEvent) -> Vec<Self::InEvent> {
		vec![event]
	}
}

impl<Pair: AsyncEventMachine> CanSubscribe<SimpleFutureMachine<Pair>> for Pair {
	fn translate_event(event: Self::InEvent) -> Vec<Pair::InEvent> {
		vec![event]
	}
}

pub struct FutureHandle<Machine: AsyncEventMachine> {
	pub sender: futures::channel::mpsc::UnboundedSender<Machine::InEvent>,
	pub subscribers: Arc<Mutex<std::collections::hash_map::HashMap<MachineId, EventSink<Machine::OutEvent>>>>,
	machine_id: MachineId,
}

impl<Machine: AsyncEventMachine> Clone for FutureHandle<Machine> {
	fn clone(&self) -> Self {
		FutureHandle {
			sender: self.sender.clone(),
			subscribers: self.subscribers.clone(),
			machine_id: self.machine_id,
		}
	}
}

pub struct FutureGuts<Machine: AsyncEventMachine> {
	pub receiver: futures::channel::mpsc::UnboundedReceiver<Machine::InEvent>,
}

impl<Machine: AsyncEventMachine> Guts<Machine> for FutureGuts<Machine> {}

impl<Machine: AsyncEventMachine> Handle<Machine> for FutureHandle<Machine> {
	fn machine_id(&self) -> MachineId {
		self.machine_id
	}
	fn broadcast_event(&self, event: Machine::OutEvent) {
		let mut failed_keys = vec![];
		let mut subscribers = self.subscribers.lock().unwrap();
		for (key, subscriber) in subscribers.iter() {
			let subscriber_result = subscriber(event.clone());
			if let Err(err) = subscriber_result {
				tracing::debug!("Error broadcasting event for subscriber {key}: {err}");
				failed_keys.push(*key);
			}
		}
		for key in failed_keys {
			subscribers.remove(&key);
		}
	}
	fn add_subscriber<Subscriber: CanSubscribe<Machine>>(&self, handle: &impl Handle<Subscriber>) -> bool {
		self.subscribers.lock().unwrap().insert(handle.machine_id(), handle.subscriber_sink()).is_none()
	}

	fn remove_subscriber<Subscriber: CanSubscribe<Machine>>(&self, handle: &impl Handle<Subscriber>) -> bool {
		self.subscribers.lock().unwrap().remove(&handle.machine_id()).is_some()
	}

	fn event_sink(&self) -> EventSink<Machine::InEvent> {
		let sender = self.sender.clone();
		Box::new(move |event| sender.unbounded_send(event).map_err(Into::into))
	}

	fn send_event(&self, event: Machine::InEvent) {
		self.sender.unbounded_send(event).unwrap();
	}
}
